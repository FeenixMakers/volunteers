module TimelineHelper

  def week_pixels
    70
  end
  def day_pixels
    week_pixels / 7
  end
  def max_days
    @weeks * 7
  end
  def start_day
    Date.today.at_beginning_of_week - 1.week
  end
  # number of days from start of month to start of stay
  def started_days(member)
    return 0 unless member.arriving
    return 0 if member.arriving < start_day
    distance = (member.arriving - start_day).to_i
    distance > max_days ? max_days : distance
  end
  def left_max
    start_day + 3.months
  end

  # amount of days of the stay, maxed at the number fo weeks shown
  def stay_days(member)
    return 0 unless member.leaving
    start = member.arriving
    start = start_day if member.arriving < start_day
    distance = ( member.leaving - start ).to_i
    max = max_days - started_days(member)
    distance > max ? max : distance
  end

  # weekly occupany, hash containing weeks to number of volunteer mapping
  def weekly
    month_start = start_day
    ( 0 ... @weeks ).collect do |week|
      amount = -1
      start_week = month_start + week.weeks
      end_week = start_week + 7.days
      #puts "WEEK #{week}  #{start_week} -- #{end_week}"
      @members.each do |mem|
        #puts "volunteer #{mem.name}"
        next unless mem.arriving
        next unless mem.leaving
        next if mem.leaving <= start_week
        #puts "volunteer #{mem.name} leaving #{mem.leaving}"
        next if mem.arriving >= end_week
        #puts "volunteer #{mem.name} arriving #{mem.arriving}"
        amount += 1
      end
      amount
    end
  end
  def bg_for(week)
    [ "bg-cyan-100",
      "bg-blue-100",
      "bg-violet-100",
      "bg-fuchsia-100",
      "bg-pink-100",
      "bg-rose-100",
      "bg-orange-100",
      "bg-amber-100",
      "bg-yellow-100",
      "bg-lime-100",
      "bg-green-100",
      "bg-teal-100",
    ][week%12]
  end
  def small_date(date)
    date.strftime("%-d.%-m")
  end
end
