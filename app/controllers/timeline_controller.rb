class TimelineController < ApplicationController

  def index
    @weeks = (params[:weeks] || "12").to_i
    @members = Member.visible_scope.
                where("leaving > ? " , Date.today).
                where("arriving < ? " , helpers.start_day + @weeks.weeks).
                order(:arriving).
                page(1).per(50)
  end
end
