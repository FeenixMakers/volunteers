class Story < ApplicationRecord
  belongs_to :member

  mount_uploader :picture, PictureUploader

  validates :text, length: { minimum: 5 , maximum: 1000 }
  validates :header , length: { minimum: 5 , maximum: 400}
  validates :picture, presence: true

  def self.ransackable_attributes(auth_object = nil)
  ["created_at", "happened", "header", "id", "member_id", "picture", "text", "updated_at"]
  end

  def name
    header
  end
end
