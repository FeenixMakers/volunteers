class Member < ApplicationRecord
  after_create :skip_conf!

  def self.public_scope
    where.not(confirmed_at: nil).where.not(picture: nil)
  end

  def self.visible_scope
    where.not(confirmed_at: nil).where.not(arriving: nil)
  end

  # Include default devise modules. Others available are:
  # , :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,:confirmable,
         :recoverable, :rememberable, :validatable, :async

  def skip_conf!
    self.confirm if Rails.env.development?
  end

  mount_uploader :picture, PictureUploader
  has_many :stories
  has_many :stories
  has_many :pictures

  validates :bio, length: { maximum: 1000 }
  validates :name , length: { minimum: 3 }
  validates_presence_of :arriving , :leaving

  def admin?
     ["torsten@villataika.fi" ,
      "ville.toivola55@gmail.com"].include?(self.email)
  end
end
