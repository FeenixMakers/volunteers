class Picture < ApplicationRecord
  belongs_to :member

  mount_uploader :picture, PictureUploader

  def self.ransackable_attributes(auth_object = nil)
  ["created_at", "happened", "id", "member_id", "picture", "text", "updated_at"]
  end
  
  validates :text, length: { maximum: 80 }
  validates :happened, presence: true
  validates :picture, presence: true

end
