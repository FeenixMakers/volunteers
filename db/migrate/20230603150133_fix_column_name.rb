class FixColumnName < ActiveRecord::Migration[7.0]
  def change
    rename_column :members, :left, :leaving
    rename_column :members, :arrived, :arriving
  end
end
