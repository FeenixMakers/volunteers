require "breeze"

# directory inside /app/assets/images where the images are kept
Breeze.images_dir = "breeze"

# directory where data and styles are kept
# Notice that the data is ALWAYS inside a breeze directory,
# so in the default case Rails.root/breeze/*.yml
Breeze.data_dir = "."
